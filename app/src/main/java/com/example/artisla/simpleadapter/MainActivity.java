package com.example.artisla.simpleadapter;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import android.view.View;
import android.widget.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity  {

    ProgressDialog pDialog;
    final String ATTRIBUTE_NAME_BUS_NUMBER = "busNumber";
    final String ATTRIBUTE_NAME_LINE_NAME = "lineName";
    final String ATTRIBUTE_NAME_GO = "going";
    final String ATTRIBUTE_NAME_RETURN = "returning";
    final String ATTRIBUTE_NAME_BUS_PERIOD = "busPeriod";
    final String ATTRIBUTE_NAME_LINE_TYPE = "lineType";
    final String URL = "http://www.kocaeli.bel.tr/services/UlasimService.aspx?service=hatlar";
    private static String bus = "";

    Intent intentBusTime;
    String busNumber,lineName,going,returning,busPeriod,lineType;
    ListView listView;

    SimpleAdapter sAdapter;

    String[] from = { ATTRIBUTE_NAME_BUS_NUMBER, ATTRIBUTE_NAME_LINE_NAME,ATTRIBUTE_NAME_GO,ATTRIBUTE_NAME_RETURN,ATTRIBUTE_NAME_BUS_PERIOD,ATTRIBUTE_NAME_LINE_TYPE };

    int [] to = { R.id.tvBusNumber, R.id.tvLineName, R.id.tvGoing,R.id.tvReturn,R.id.tvBusPeriod,R.id.tvLineType };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(!isConnected()){
            Toast.makeText(this,"Internet Connection Failed!",Toast.LENGTH_SHORT).show();

        }

        listView = (ListView) findViewById(R.id.listView);

        ActionBar actionBar = getActionBar();
        actionBar.setTitle(Html.fromHtml("<font color='#ffffff'>Kocaeli Bus</font>"));
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3f51b5")));


        new AsyncTaskParseJson().execute();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String busNumber = ((TextView) view.findViewById(R.id.tvBusNumber)).getText().toString();
                String lineName = ((TextView) view.findViewById(R.id.tvLineName)).getText().toString();
                showBusTime(lineName, busNumber);
            }
        });
    }

    public void showBusTime(String lineName,String busNumber){
        intentBusTime = new Intent(this,BusTime.class);
        intentBusTime.putExtra("busNumber",busNumber);
        intentBusTime.putExtra("lineName",lineName);
        startActivity(intentBusTime);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.options_menu, menu);

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView)menu.findItem(R.id.menu_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextChange(String newText){
            sAdapter.getFilter().filter(newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query){
                sAdapter.getFilter().filter(query);
                return true;
            }
        };

        searchView.setOnQueryTextListener(textChangeListener);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    ArrayList<Map<String,String>> data = new ArrayList<Map<String, String>>();
    private void initList(){
        try{
            if(!bus.isEmpty()){

                JSONObject reader = new JSONObject(bus);
                JSONArray jsonArray = reader.optJSONArray("Hatlar");
                Map<String, String> m;

                for(int k=0;k<jsonArray.length();k++) {
                    JSONObject jsonObject= jsonArray.getJSONObject(k);
                    busNumber = jsonObject.getString("hat_kodu");
                    lineName = jsonObject.getString("hat_adi");
                    busPeriod = jsonObject.getString("sefer_muddeti");
                    going = jsonObject.getString("gidis");
                    returning = jsonObject.getString("donus");
                    lineType = jsonObject.getString("hat_cinsi");


                    m = new HashMap<String, String>();

                    m.put(ATTRIBUTE_NAME_BUS_NUMBER,busNumber);
                    m.put(ATTRIBUTE_NAME_LINE_NAME,lineName);
                    m.put(ATTRIBUTE_NAME_GO,"Gidiş: "+going);
                    m.put(ATTRIBUTE_NAME_RETURN,"Dönüş: "+returning);
                    m.put(ATTRIBUTE_NAME_BUS_PERIOD,"Sefer Müddeti: "+busPeriod+" dk");
                    m.put(ATTRIBUTE_NAME_LINE_TYPE,"Hat Cinsi: "+lineType);
                    data.add(m);}
            }
            else {
                Toast.makeText(this,"bus variable empty !!!",Toast.LENGTH_LONG).show();
            }
        }
        catch(JSONException e){
            e.printStackTrace();
        }
    }



    public void showBusLine() {
        initList();
        sAdapter = new SimpleAdapter(this, data, R.layout.item, from, to);
        listView.setAdapter(sAdapter);
    }

    public  boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    public class AsyncTaskParseJson extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Loading Bus Line !!!");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(String... arg0) {

            try {
                JsonParser jParser = new JsonParser();
                bus = jParser.getJSONFromUrl(URL);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String strFromDoInBg) {

            showBusLine();
            pDialog.dismiss();

        }
    }
}
