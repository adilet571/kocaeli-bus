package com.example.artisla.simpleadapter;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.widget.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BusTime extends Activity {

    ActionBar.Tab goTab,  returningTab;
    static   Fragment goFragment, returningFragment;
    static   String urlBusNumber;
    public    String busNumber;
    static   ActionBar actionBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_time);

        Intent intent = getIntent();

        String lineName = intent.getStringExtra("lineName");
        busNumber = intent.getStringExtra("busNumber");
        urlBusNumber = "http://www.kocaeli.bel.tr/services/UlasimService.aspx?service=saatler&hatkodu="+ URLEncoder.encode(busNumber);

         goFragment = new goFragmentTab();
         returningFragment = new returningFragmentTab();



        actionBar = getActionBar();
        actionBar.setTitle(Html.fromHtml(String.format("<font color='#ffffff'>%s</font>", lineName)));
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3f51b5")));
        // Screen handling while hiding ActionBar icon.
        actionBar.setDisplayShowHomeEnabled(false);

        // Creating ActionBar tabs.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Setting custom tab icons.
        goTab = actionBar.newTab().setText(R.string.go);
        returningTab = actionBar.newTab().setText(R.string.returning);

        // Setting tab listeners.

        goTab.setTabListener(new TabListener(goFragment));
        returningTab.setTabListener(new TabListener(returningFragment));
        // Adding tabs to the ActionBar.
        actionBar.addTab(goTab);
        actionBar.addTab(returningTab);





    }

    public class TabListener implements ActionBar.TabListener
    {

        private  Fragment fragment;

        // The contructor.
        public TabListener(Fragment fragment) {
            this.fragment = fragment;
        }

        // When a tab is tapped, the FragmentTransaction replaces
        // the content of our main layout with the specified fragment;
        // that's why we declared an id for the main layout.
        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            ft.replace(R.id.activity_main, fragment);
            switch (tab.getPosition()){
                case 0:
                    Log.d("Go tab","go tab");
                    break;
                case 1:
                    Log.d("Return tab", "return tab");
                    break;
            }


        }

        // When a tab is unselected, we have to hide it from the user's view.
        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            ft.remove(fragment);
        }

        // Nothing special here. Fragments already did the job.
        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

        }

    }



}
