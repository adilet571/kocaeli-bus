package com.example.artisla.simpleadapter;

/**
 * Created by artisla on 7/28/15.
 */import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JsonParser {

    static InputStream is = null;
    static String json = "";
    DefaultHttpClient httpClient;
    HttpPost httpPost;
    HttpResponse httpResponse;
    HttpEntity httpEntity;


    public String getJSONFromUrl(String url) {


        try {
            httpClient = new DefaultHttpClient();
            httpPost = new HttpPost(url);
            httpResponse = httpClient.execute(httpPost);
            httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();



            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        finally {
            httpClient.getConnectionManager().shutdown();
        }
        return json;

    }
}
