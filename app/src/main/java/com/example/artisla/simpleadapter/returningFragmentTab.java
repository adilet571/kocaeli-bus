package com.example.artisla.simpleadapter;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class returningFragmentTab extends Fragment {


    String busTime;
    ListView lvReturning;
    List workDay,saturday,sunday;
    ProgressDialog pDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.returning_layout, container, false);

        lvReturning = (ListView)rootView.findViewById(R.id.lvReturning);
        workDay = new ArrayList();
        saturday = new ArrayList();
        sunday = new ArrayList();

        try {
            new AsyncTaskParseJson().execute();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return rootView;
    }

    public void showBusLine() {
        initList("Gidis");
        SimpleAdapter simpleAdapter = new SimpleAdapter(returningFragmentTab.this.getActivity(), employeeList, android.R.layout.simple_list_item_2, new String[] {"Donus"}, new int[] {android.R.id.text2});
        Log.d("employee list", employeeList.toString());
        lvReturning.setAdapter(simpleAdapter);

    }
    List<Map<String,String>> employeeList = new ArrayList<Map<String,String>>();
    private void initList(String lineType){
        String outPut;
        String gidis, workDayString="", saturdayString="", sundayString="";

        try{
            if(!busTime.isEmpty()){
                Log.d("in goFragment url ",BusTime.urlBusNumber);
                Log.d("in goFragmentClass",busTime);
                JSONObject reader = new JSONObject(busTime);
                JSONArray jsonArray = reader.optJSONArray("Tables");
                JSONObject jsonObject = jsonArray.getJSONObject(2);
                JSONArray  jsonArray1 = jsonObject.getJSONArray("Donus");


                for(int k =0 ;k<jsonArray1.length();k++){
                    JSONObject jsonObject1 = jsonArray1.getJSONObject(k);

                    gidis = jsonObject1.getString("Donus");

                    if(jsonObject1.getString("GunTipi").equals("C"))
                    { saturday.add(gidis);
                    }
                    if(jsonObject1.getString("GunTipi").equals("P"))
                    {     sunday.add(gidis);}
                    if(jsonObject1.getString("GunTipi").equals("I"))
                    { workDay.add(gidis); }
                }

                int max = Math.max(Math.max(saturday.size(), sunday.size()), workDay.size());

                employeeList.clear();
                employeeList.add(createEmployee("Donus", "İş Günleri      Cumartesi        Pazar"));
                for(int k=0;k<max;k++){
                    if(workDay.size()>k)
                        workDayString = workDay.get(k).toString();
                    else
                        workDayString = "--   :   --";
                    if(saturday.size()>k)
                        saturdayString = saturday.get(k).toString();
                    else
                        saturdayString = "--   :   --";
                    if (sunday.size()>k)
                        sundayString = sunday.get(k).toString();
                    else
                        sundayString = "--   :   --";

                    outPut =(workDayString +"              "+saturdayString+"              "+sundayString);

                    employeeList.add(createEmployee("Donus",outPut));
                }
            }
        }
        catch(JSONException e){
            e.printStackTrace();
        }
    }

    private HashMap<String, String> createEmployee(String name,String number){
        HashMap<String, String> employeeNameNo = new HashMap<String, String>();
        employeeNameNo.put(name, number);
        return employeeNameNo;
    }


    public class AsyncTaskParseJson extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {

            pDialog = new ProgressDialog(returningFragmentTab.this.getActivity());
            pDialog.setMessage("Loading Bus Time !!!");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                JsonParser jParser = new JsonParser();
                busTime = jParser.getJSONFromUrl(BusTime.urlBusNumber);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String strFromDoInBg) {
            showBusLine();
            pDialog.dismiss();
        }
    }
}
